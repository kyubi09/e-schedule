<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

		public function __construct()
		{
		parent::__construct();
		$this->load->model('login_model');
		}

		public function index()
		{
			if (!$this->session->userdata('is_logged_in'))
			{
			$data['salah']=0;
			$this->load->view('login',$data);
			}
	    else
			{
						redirect('');
	    }
		}

		/**
    * encript the password
    * @return mixed
    */
    function __encrip_password($password)
    {
      return md5($password);
    }

		function validate_credentials()
	{


		$username = $this->input->post('username');
		$password = $this->__encrip_password($this->input->post('password'));
		//echo $username;
		$is_valid = $this->login_model->validate($username, $password);

		//echo var_dump($is_valid);
		if($is_valid)
		{
			$userdate=$this->login_model->getdata($username);
			$userkode=$userdate[0]['userkode'];
			$data = array(
				'user' => $username,
				'userkode' =>$userkode,
				'is_logged_in' => true
			);
			//echo var_dump($userdate);
			//echo $username;

			$this->session->set_userdata($data);
	     redirect('');
		}
		else // incorrect username or password
		{

			$data['salah']=1;
			//$data['message_error'] = TRUE;
			$this->load->view('login',$data);
		}
	}

	function logout()
		{
			$this->session->sess_destroy();
			redirect('login');
		}

}
