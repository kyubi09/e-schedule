<?php

	class Jadwal_model extends CI_Model
	{

		public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}
  function tambah($data)
	{
  $res = $this->db->insert('jadwal', $data);
	return  $res;
	//return $res;
	}



	function terpadukabadan($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,kabadan');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('kabadan is NOT NULL', NULL, FALSE);
		$query=$this->db->get('jadwal');
		$this->db->order_by('waktu', "asc");
		return $query->result_array();

	}

	function terpaduses($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,ses');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('ses is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();

	}

	function terpadufungham($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,fungham');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('fungham is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();
	}

	function terpadupenkom($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,penkom');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('penkom is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();

	}
	function terpadutekpim($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,tekpim');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('tekpim is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();

	}

	function terpadupoltekip($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,poltekip');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('poltekip is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();

	}

	function terpadupoltekim($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,poltekim');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('poltekim is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();

	}

	function privatekabadan($awal,$akhir)
	{

		return $this->db
				->select('*')
				->where('tanggal >=',$awal)
				->where('tanggal <=',$akhir)
	     ->group_start()
	         ->where('kabadan is NOT NULL', NULL, FALSE)
	         ->or_where('dibuat', "tukabadan")
	     ->group_end()
	     ->get('jadwal')
	     ->result_array();
	}

	function privateses($awal,$akhir)
	{
		return $this->db
				->select('*')
				->where('tanggal >=',$awal)
				->where('tanggal <=',$akhir)
			 ->group_start()
					 ->where('ses is NOT NULL', NULL, FALSE)
					 ->or_where('dibuat', "tuses")
			 ->group_end()
			 ->get('jadwal')
			 ->result_array();
	}

	function privatefungham($awal,$akhir)
	{
		return $this->db
				->select('*')
				->where('tanggal >=',$awal)
				->where('tanggal <=',$akhir)
			 ->group_start()
					 ->where('fungham is NOT NULL', NULL, FALSE)
					 ->or_where('dibuat', "tufungham")
			 ->group_end()
			 ->get('jadwal')
			 ->result_array();
		}
	function privatepenkom($awal,$akhir)
	{
		return $this->db
				->select('*')
				->where('tanggal >=',$awal)
				->where('tanggal <=',$akhir)
			 ->group_start()
					 ->where('penkom is NOT NULL', NULL, FALSE)
					 ->or_where('dibuat', "tupenkom")
			 ->group_end()
			 ->get('jadwal')
			 ->result_array();
	}

	function privatetekpim($awal,$akhir)
	{
		return $this->db
				->select('*')
				->where('tanggal >=',$awal)
				->where('tanggal <=',$akhir)
			 ->group_start()
					 ->where('tekpim is NOT NULL', NULL, FALSE)
					 ->or_where('dibuat', "tutekpim")
			 ->group_end()
			 ->get('jadwal')
			 ->result_array();
		}
	function privatepoltekip($awal,$akhir)
	{
		return $this->db
				->select('*')
				->where('tanggal >=',$awal)
				->where('tanggal <=',$akhir)
			 ->group_start()
					 ->where('poltekip is NOT NULL', NULL, FALSE)
					 ->or_where('dibuat', "tupoltekip")
			 ->group_end()
			 ->get('jadwal')
			 ->result_array();
		}

	function privatepoltekim($awal,$akhir)
	{
		return $this->db
				->select('*')
				->where('tanggal >=',$awal)
				->where('tanggal <=',$akhir)
			 ->group_start()
					 ->where('poltekim is NOT NULL', NULL, FALSE)
					 ->or_where('dibuat', "tupoltekim")
			 ->group_end()
			 ->get('jadwal')
			 ->result_array();
		}

		function update($data,$id)
		{
		$this->db->where('id', $id);
		$res = $this->db->update('jadwal', $data);
		return  $res;
		//return $res;
		}

		function deletefile($id)
		{
		$this->db->set('file', NULL);
		$this->db->where('id', $id);
		$res = $this->db->update('jadwal');
		return  $res;
		//return $res;
		}

		function getlain($id)
		{
			$this->db->select('lain');
			$this->db->where('id',$id);
			$query=$this->db->get('jadwal');
			return $query->result_array();

		}
		function getbyid($id)
		{
			$this->db->select('*');
			$this->db->where('id',$id);
			$query=$this->db->get('jadwal');
			return $query->result_array();

		}
		function delete($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('jadwal');
		}

		function cekpass($lama, $username)
		{
	  $this->db->where('username', $username);
	  $this->db->where('password', $lama);
	  $query = $this->db->get('user');

	  if($query->num_rows() == 1)
	  	{
	    return true;
	  	}
		}

		function gantipass($data,$username)
		{
		$this->db->where('username', $username);
		$res = $this->db->update('user', $data);
		return  $res;
		//return $res;
		}

		function searchkabadan()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('kabadan is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tukabadan");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}

		function searchses()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('ses is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tuses");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}
		function searchfungham()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('fungham is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tufungham");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}

		function searchpenkom()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('penkom is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tupenkom");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}
		function searchtekpim()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('tekpim is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tutekpim");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}

		function searchpoltekip()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('poltekip is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tupoltekip");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}

		function searchpoltekim()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('poltekim is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tupoltekim");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}

		function searchbyid($id)
		{
			$this->db->select('*');
			$this->db->where('id',$id);
			$query=$this->db->get('jadwal');
			return $query->result_array();
			}

}
