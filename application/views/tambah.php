      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Tambah Kegiatan</small>
          </h1>
         </section>

        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
                <h3>
                  Tambah Kegiatan
                </h3>
                <form action="#" role="form" method="post" enctype="multipart/form-data">
                  <div class="box-body">

                    <div class="form-group col-xs-6">
                      <label>Hari Tanggal</label>
                      <div class="input-group date col-xs-12" data-date-format="yyyy-mm-dd">
                        <input  name='tanggal' type="text" class="form-control" placeholder="dd.mm.yyyy">
                        <div class="input-group-addon" >
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-xs-6">
                      <label>Waktu</label>
                      <input name='waktu' type="text" class="form-control" placeholder="xx:xx AM s/d xx:xx PM"/>
                    </div>

                    <div class="form-group col-xs-12">
                      <label>Kegiatan</label>
                      <input name='kegiatan' type="text" class="form-control" placeholder="Enter ..."/>
                    </div>

                    <div class="form-group col-xs-12">
                      <label>Tempat</label>
                      <input name='tempat' type="text" class="form-control" placeholder="Enter ..."/>
                    </div>

                    <div class="form-group col-xs-12">
                      <label>Materi</label>
                      <input type="text" name='materi' class="form-control" placeholder="Enter ..."/>
                    </div>


                    <div class="form-group col-xs-12">
                      <label>Pejabat Turut Diundang</label>
                      <div class="checkbox">
                        <?php $user=$this->session->userdata('user'); ?>
                        <label class="col-xs-3"> <input name='kabadan' type="checkbox" <?php if ($user=="tukabadan") { ?>  checked <?php  } ?>/> Kepala BPSDM </label>
                        <label class="col-xs-3"> <input name='ses' type="checkbox" <?php if ($user=="tuses")  { ?>   checked<?php  } ?>/> Sekretaris BPSDM </label>
                        <label class="col-xs-3"> <input name='tekpim' type="checkbox" <?php if ($user=="tutekpim")  { ?>   checked <?php  } ?> /> Ka. PusTekPim </label>
                        <label class="col-xs-3"> <input name='fungham' type="checkbox" <?php if ($user=="tufungham")  { ?>   checked <?php  } ?> /> Ka. PusFungHam </label>
                        <label class="col-xs-3"> <input name='penkom' type="checkbox" <?php if ($user=="tupenkom")  { ?>   checked <?php  } ?> /> Ka. PusPenKom </label>
                        <label class="col-xs-3"> <input name='poltekip' type="checkbox" <?php if ($user=="tupoltekip")  { ?>   checked <?php  } ?>/> Dir. Poltekip </label>
                        <label class="col-xs-3"> <input name='poltekim' type="checkbox" <?php if ($user=="tupoltekim")  { ?>   checked <?php  } ?>/> Dir. Poltekim </label>
                        <input type="text" class="col-xs-3" name='lain' placeholder="Lainnya"/>
                      </div>
                    </div>

                    <div class="form-group col-xs-4">
                      <label>Contact Person</label>
                      <input type="text" class="form-control" name='cp' placeholder="Enter ..."/>
                    </div>
                    <div class="form-group col-xs-4">
                      <label>Dress Code</label>
                      <input type="text" class="form-control" name='dresscode' placeholder="Enter ..."/>
                    </div>
                    <div class="form-group col-xs-4">
                      <label>Kategori</label>
                      <input type="text" class="form-control" name='kategori' placeholder="Enter ..."/>
                    </div>

                    <div class="form-group col-xs-12">
                      <label>Keterangan Lain</label>
                      <input type="text" class="form-control" name='keterangan' placeholder="Enter ..."/>
                    </div>

                    <div class="col-md-12">
      								<div class="form-group">
      										<label>File <span class="text-hightlight"></span></label>
      										<input type="file" class="form-control" name="dokumen"/>
      								</div>
      							</div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" value='upload'>Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->


            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->

        <!-- Main content -->
      </div><!-- /.content-wrapper -->
