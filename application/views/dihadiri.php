      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <section class="content">
                  <div class="row ">
                    <div class="col-xs-12 ">
                      <div class="box bg-gray disabled color-palette">
                        <div class="box-header">
                          <h3 class="box-title">Jadwal Kegiatan Jabatan Pimpinan</h3>
                        </div><!-- /.box-header -->
                        <div class="btn-gorup">
                          <a href="<?php echo base_url(); ?>jadwal/pimpinan" class="btn btn-info"> All Agenda </a>
                          <!--<a href="<?php echo base_url(); ?>jadwal/dihadiri" class="btn btn-info"> Agenda Yang Dihadiri</a> -->
                          <a href="<?php echo base_url(); ?>jadwal/cetak" class="btn btn-info"> Cetak </a>
                          <a href="<?php echo base_url(); ?>jadwal/tambah" class="btn btn-info pull-right"> + Tambah </a>
                        </div>
                        <div class="box-body">
                          <table class="table table-hover" border='2'>
                            <tr class="bg-maroon color-palette">
                              <th style="width:10%">Hari/Tanggal</th>
                              <th style="width:10%">Waktu</th>
                              <th style="width:10%">Kegiatan</th>
                              <th style="width:10%">Tempat</th>
                              <th style="width:10%">Keterangan</th>
                              <th style="width:10%">Disposisi/Tindak Lanjut</th>
                              <th style="width:10%">Option</th>
                            </tr>
                            <tr>
                              <td>Senin / 17 Nov 2017 </td>
                              <td>12.00 PM</td>
                              <td>Rapat Koordinasi</td>
                              <td>Ruang Bima Kemenkopolhukam</span></td>
                              <td>
                                Materi :  <br>
                                Pejabat Turut Diundang :  <br>
                                Contact Person :  <br>
                                Dress Code : <br>
                                Lain-lain : <br>
                                Undangan
                              </td>
                              <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                              <td>
                                <a href="#"  class="btn btn-block btn-info btn sm"> Update </a><br>
                                <a href="#"  class="btn btn-block btn-danger btn sm"> Hapus </a> <br>
                                Dibuat oleh : <br>
                                Last Update .... oleh .....
                              </td>
                            </tr>
                          </table>
                        </div><!-- /.box-body -->
                      </div><!-- /.box -->
                    </div>
                  </div>
                </section>



        <!-- Main content -->
      </div><!-- /.content-wrapper -->
