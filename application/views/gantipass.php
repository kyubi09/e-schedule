      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Ganti Password</small>
          </h1>
         </section>

        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
                <h3>
                  Ganti Password
                </h3>
                <form action="#" role="form" method="post">

                    <div class="form-group col-xs-12">
                      <label>User</label>
                      <select name='user' class="form-control">
                        <option value="tukabadan">TU Kabadan</option>
                        <option value="tuses">TU Sekretaris</option>
                        <option value="tufungham">TU PusFungHAM</option>
                        <option value="tupenkom">TU PusPenKom</option>
                        <option value="tutekpim">TU PusTekPim</option>
                        <option value="tupoltekim">TU Poltekim</option>
                        <option value="tupoltekip">TU Poltekip</option>
                      </select>


                    <div class="form-group has-feedback">
                      <label>Password Baru</label>
                      <input type="password" name='password' class="form-control" placeholder="Password Baru" id="password" required/>
                      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                      <label>Masukkan Kembali Password Baru Anda</label>
                      <input type="password" class="form-control" placeholder="Confirm Password" id="confirm_password" required/>
                      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" id='Button' class="btn btn-primary">Ganti Password</button>
                  </div>
                </form>
              </div><!-- /.box -->
               <script type="text/javascript">
                var password = document.getElementById("password");
                var confirm_password = document.getElementById("confirm_password");

                function validatePassword(){
                  if(password.value != confirm_password.value) {
                    document.getElementById("Button").disabled = true;
                  } else {
                  document.getElementById("Button").disabled = false;
                  }
                }

                password.onchange = validatePassword;
                confirm_password.onkeyup = validatePassword;
               </script>

            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->

        <!-- Main content -->
      </div><!-- /.content-wrapper -->
