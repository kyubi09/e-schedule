      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Tambah Kegiatan</small>
          </h1>
         </section>

        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
                <h3>
                  Update Kegiatan
                </h3>
                <form action="#" role="form" method="post" enctype="multipart/form-data">
                  <div class="box-body">

                    <div class="form-group col-xs-6">
                      <label>Hari Tanggal</label>
                      <div class="input-group date col-xs-12" data-date-format="yyyy-mm-dd">
                        <input  name='tanggal' type="text" class="form-control" value="<?php echo $tanggal ;?>">
                        <div class="input-group-addon" >
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-xs-6">
                      <label>Waktu</label>
                      <input name='waktu' type="text" class="form-control" value="<?php echo $waktu ;?>"/>
                    </div>

                    <div class="form-group col-xs-12">
                      <label>Kegiatan</label>
                      <input name='kegiatan' type="text" class="form-control" value="<?php echo $kegiatan ;?>"/>
                    </div>

                    <div class="form-group col-xs-12">
                      <label>Tempat</label>
                      <input name='tempat' type="text" class="form-control" value="<?php echo $tempat ;?>"/>
                    </div>

                    <div class="form-group col-xs-12">
                      <label>Materi</label>
                      <input type="text" name='materi' class="form-control" value="<?php echo $materi ;?>"/>
                    </div>


                    <div class="form-group col-xs-12">
                      <label>Pejabat Turut Diundang</label>
                      <div class="col-xs-12" >
                      <div class="form-group col-xs-4">
                        <label>Kepala BPSDM</label>
                        <select name='kabadan' class="form-control">
                          <option value="<?php if ($kabadan) { echo $kabadan; } else { echo "1"; } ?>" <?php if ($kabadan) { echo "selected"; } ?>>Diikutsertakan</option>
                          <option value='tidak' <?php if (!$kabadan) { echo "selected"; } ?> >Tidak Diikutsertakan</option>
                        </select>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Sekretaris BPSDM</label>
                        <select name='ses' class="form-control">
                          <option value="<?php if ($ses) { echo $ses; } else { echo "1"; } ?>" <?php if ($ses) { echo "selected"; } ?>>Diikutsertakan</option>
                          <option value='tidak' <?php if (!$ses) { echo "selected"; } ?>>Tidak Diikutsertakan</option>
                        </select>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Ka. PusFungHam</label>
                        <select name='fungham' class="form-control">
                          <option value="<?php if ($fungham) { echo $fungham; } else { echo "1"; } ?>" <?php if ($fungham) { echo "selected"; } ?>>Diikutsertakan</option>
                          <option value='tidak' <?php if (!$fungham) { echo "selected"; } ?>>Tidak Diikutsertakan</option>
                        </select>
                      </div>
                      </div>

                      <div class="col-xs-12" >
                      <div class="form-group col-xs-4">
                        <label>Ka. PusPenKom</label>
                        <select name='penkom' class="form-control">
                          <option value="<?php if ($penkom) { echo $penkom; } else { echo "1"; } ?>" <?php if ($penkom) { echo "selected"; } ?>>Diikutsertakan</option>
                          <option value='tidak' <?php if (!$penkom) { echo "selected"; } ?>>Tidak Diikutsertakan</option>
                        </select>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Ka. PusTekPim</label>
                        <select name='tekpim' class="form-control">
                          <option value="<?php if ($tekpim) { echo $ses; } else { echo "1"; } ?>" <?php if ($tekpim) { echo "selected"; } ?>>Diikutsertakan</option>
                          <option value='tidak' <?php if (!$tekpim) { echo "selected"; } ?>>Tidak Diikutsertakan</option>
                        </select>
                      </div>
                      <div class="form-group col-xs-4">
                        <label>Dir. Poltekip</label>
                        <select name='poltekip' class="form-control">
                          <option value="<?php if ($poltekip) { echo $poltekip; } else { echo "1"; } ?>" <?php if ($poltekip) { echo "selected"; } ?>>Diikutsertakan</option>
                          <option value='tidak' <?php if (!$poltekip) { echo "selected"; } ?>>Tidak Diikutsertakan</option>
                        </select>
                      </div>
                      </div>

                      <div class="col-xs-12" >
                        <div class="form-group col-xs-4">
                          <label>Dir. Poltekim</label>
                          <select name='poltekim' class="form-control">
                            <option value="<?php if ($poltekim) { echo $poltekim; } else { echo "1"; } ?>" <?php if ($poltekim) { echo "selected"; } ?>>Diikutsertakan</option>
                            <option value='tidak' <?php if (!$poltekim) { echo "selected"; } ?>>Tidak Diikutsertakan</option>
                          </select>
                        </div>
                      </div>


                    </div>

                    <div class="form-group col-xs-4">
                      <label>Contact Person</label>
                      <input type="text" class="form-control" name='cp' value="<?php echo $cp ;?>"/>
                    </div>
                    <div class="form-group col-xs-4">
                      <label>Dress Code</label>
                      <input type="text" class="form-control" name='dresscode' value="<?php echo $dresscode ;?>"/>
                    </div>
                    <div class="form-group col-xs-4">
                      <label>Kategori</label>
                      <input type="text" class="form-control" name='kategori' value="<?php echo $kategori ;?>"/>
                    </div>

                    <div class="form-group col-xs-12">
                      <label>Keterangan Lain</label>
                      <input type="text" class="form-control" name='keterangan' value="<?php echo $keterangan ;?>"/>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                          <label>File <span class="text-hightlight"></span></label>
                          <input type="file" class="form-control" name="dokumen"/>
                      </div>
                    </div>

                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->


            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->

        <!-- Main content -->
      </div><!-- /.content-wrapper -->
